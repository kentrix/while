use crate::lexer::{SyntaxKind, Lexer};
use logos::Logos;
use rowan::{GreenNodeBuilder, GreenNode, Language, Checkpoint};
use crate::syntax::{WhileLanguage, SyntaxNode};
use std::iter::Peekable;

trait BindingPower {
    fn binding_power(&self) -> (u8, u8);
}

pub enum Op {
    Add,
    Sub,
    Mul,
    Div,
}

pub enum UnaryOp {
    Neg,
    Not,
}

impl Op {
    fn binding_power(&self) -> (u8, u8) {
        match self {
            Self::Add | Self::Sub => (1, 2),
            Self::Mul | Self::Div => (3, 4),
        }
    }
}

impl  UnaryOp {
    fn binding_power(&self) -> ((), u8) {
        match self {
            Self::Neg => ((), 5),
            Self::Not => ((), 6),
        }
    }
}

pub(crate) struct Parser<'a> {
    lexer: Peekable<Lexer<'a>>,
    builder: GreenNodeBuilder<'static>,
}


impl<'a> Parser<'a> {
    pub(crate) fn new(input: &'a str) -> Self {
        Self {
            lexer: Lexer::new(input).peekable(),
            builder: GreenNodeBuilder::new(),
        }
    }

    pub(crate) fn parse(mut self) -> Parse {
        self.start_node(SyntaxKind::Root.into());

        self.expr();

        self.finish_node();

        Parse {
            green_node: self.builder.finish(),
        }
    }

    fn peek(&mut self) -> Option<SyntaxKind> {
        self.lexer.peek().map(|(kind, _)| *kind)
    }

    fn consume(&mut self) {
        let (kind, text) = self.lexer.next().unwrap();

        self.builder
            .token(WhileLanguage::kind_to_raw(kind), text.into());
    }

    fn start_node(&mut self, kind: SyntaxKind) {
        self.builder.start_node(WhileLanguage::kind_to_raw(kind));
    }

    fn finish_node(&mut self) {
        self.builder.finish_node();
    }

    fn start_node_at(&mut self, checkpoint: Checkpoint, kind: SyntaxKind) {
        self.builder
            .start_node_at(checkpoint, WhileLanguage::kind_to_raw(kind));
    }

    fn checkpoint(&self) -> Checkpoint {
        self.builder.checkpoint()
    }

    fn eat_whitespace(&mut self) {
        if self.peek() == Some(SyntaxKind::Whitespace) {
            self.consume();
        }
    }

    fn expr(&mut self) {
        self.expr_binding_power(0)
    }

    fn expr_binding_power(&mut self, minimum_binding_power: u8) {
        self.eat_whitespace();
        let checkpoint = self.checkpoint();
        match self.peek() {
            Some(SyntaxKind::Number)
            | Some(SyntaxKind::Ident)
            | Some(SyntaxKind::Spec)
            | Some(SyntaxKind::True)
            | Some(SyntaxKind::False) => self.consume(),
            Some(SyntaxKind::Not)
            | Some(SyntaxKind::Minus) => {
                let op = if self.peek() == SyntaxKind::Not {
                    UnaryOp::Not
                } else {
                    UnaryOp::Neg
                };
                let ((), right_binding_power) = op.binding_power();

                self.consume();

                self.start_node_at(checkpoint, SyntaxKind::UnaryExpr);
                self.expr_binding_power( right_binding_power);
                self.finish_node();
            }
            Some(SyntaxKind::LParen) => {
                self.consume();
                self.expr_binding_power( 0);

                assert_eq!(self.peek(), Some(SyntaxKind::RParen));
                self.consume();
            }
            _ => {}
        }

        loop {
            self.eat_whitespace();
            let op = match self.peek() {
                Some(SyntaxKind::Plus) => Op::Add,
                Some(SyntaxKind::Minus) => Op::Sub,
                Some(SyntaxKind::Star) => Op::Mul,
                Some(SyntaxKind::Slash) => Op::Div,
                _ => return,
            };

            let (left_binding_power, right_binding_power) = op.binding_power();

            if left_binding_power < minimum_binding_power {
                return;
            }

            // Eat the operator’s token.
            self.consume();

            self.start_node_at(checkpoint, SyntaxKind::BinaryExpr);
            self.expr_binding_power(right_binding_power);
            self.finish_node();
        }
    }

}

pub(crate) struct Parse {
    pub green_node: GreenNode,
}

impl Parse {
    pub fn debug_tree(&self) -> String {
        let syntax_node = SyntaxNode::new_root(self.green_node.clone());
        let formatted = format!("{:#?}", syntax_node);

        formatted[0..formatted.len() - 1].to_string()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::syntax::SyntaxNode;

    #[test]
    fn parse_nothing() {
        let parse = Parser::new("").parse();

        assert_eq!(
            format!("{:#?}", SyntaxNode::new_root(parse.green_node)),
            r#"Root@0..0
"#,
        );
    }
}
