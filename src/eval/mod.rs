use crate::parser::Parse;
use rowan::{GreenNode, };
use crate::lexer::SyntaxKind;

struct Eval {

}
impl Eval {
    pub fn prune_white_space(p: &GreenNode) {

    }
    pub fn eval(p: &GreenNode) {
        match p.kind() as SyntaxKind {
            SyntaxKind::Root => {
                for j in p.children() {
                    Eval::eval(j.into())
                }
            }
            SyntaxKind::BinaryExpr => {
                let mut children = p.children();
                let fst = children.next().unwrap();
                let fst_eval = Eval::eval(fst.into());
                let op = children.next().unwrap();
            }
        }
    }


}